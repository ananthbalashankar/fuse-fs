#ifndef MYFS_H_INCLUDED
#define MYFS_H_INCLUDED

#define FS_Name "myFS" // filesystem name
#define HOME "root" // home filesystem
#define FILENAMESIZE 50 // Presently keeping size fixed - to be decided what is to be done for filename.
#define FILESYSSIZE 102400 // in bytes 100kB
#define BLKSIZE 64 // block size in bytes
#define INODESIZE 200 // no. of inodes
#define NUMDIRECTBLK 8 // no. of direct blocks
#define NUMINDIRECTBLK 2 // no. of indirect blocks
#define FREESPCSIZE 2*1024 // in bytes
#define MAXFILES 170

#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>
#include<time.h>
#include<sys/stat.h>

typedef struct ind // inode structure
{
    // File Attributes
    char fname[FILENAMESIZE];
    char type;                              // 'f' - for file, 'd' - for directory
    int size;                               // in bytes
    time_t creation_time;
    char mode;                               // r - readonly, w - read-write, i - invalid,  any further mode to be added, mode valid for regular file only
    int blklist[NUMDIRECTBLK+NUMINDIRECTBLK];  
    int inext;                                // inode number for directories and files at the same level
    int isubdir;                             // for subdirectories  
    int inode_num;
    // Array of pointers to be added here
}inode;

typedef struct superblk
{
    char filesysname[sizeof(FS_Name)];
    int filesyssize; // Check for the case when file system size exceeds integer range
    int rootinode; 
    int used_blks;  // no. of used blocks
    int free_blks;  // no. of free blocks
    int free_inode; // no. of free inodes
    int ilistoff;   // offset of the inode list from SEEK_SET
    int ifreelstoff; // offset of freelist of inodes
    int numiblk;    // no. of blocks required to store each inode
    // Other Entries to be added here in future
}superblock;

typedef struct ilst
{
    inode i;
    struct ilst *next;
    struct ilst *subdir;
}ilist;

typedef struct
{
    ilist* inlst;
    int offset;
    char *path;
}fdte;

superblock s;
int freelistlen; // global fd opens in mount closed in unmount
char freeinode[INODESIZE], *freeblklist;
ilist *head;
fdte oft[MAXFILES];

void myformat();
int mymount(char *fsname);
int unmount(char *fsname);
int myopen(char *fname, char mode);
int myread(int filedes, void *buf, int count);
int mywrite(int filedes, void *buf, int count);
int myclose(int filedescriptor);
int mymkdir(const char *dirname, mode_t mode);
int myrmdir(char *dirname);
int myrm(char *dirname);
int myls(const char *dirname);

#endif
