/*
 *  FUSE: Filesystem in Userspace
 *  Copyright (C) 2001-2007  Miklos Szeredi <miklos@szeredi.hu>
 * 
 *  This program can be distributed under the terms of the GNU GPL.
 *  See the file COPYING.
 * 
 * gcc -Wall `pkg-config fuse --cflags --libs` filesystem.c -o filesystem
 * 
 */

#define FUSE_USE_VERSION 26

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef linux
/* For pread()/pwrite() */
#define _XOPEN_SOURCE 500
#endif

#include "myfuseFS.h"
#include <fuse.h>
#include <limits.h>
#include <dirent.h>
#include <sys/time.h>
#include<sys/stat.h>

#define FS_DATA ((struct bb_state *) fuse_get_context()->private_data)
int mountflg = 1, fd = 0;

int stringparse(char **tokens, const char *dirname)
{
    int count=0;
    char *str, *strtemp;
    str = (char *)malloc(sizeof(char)*(strlen(dirname)+1));
    strcpy(str, dirname);
    str[strlen(dirname)] = '\0';

    strtemp = strtok(str, "/");
    tokens[0] = (char *)malloc(sizeof(char)*(strlen(strtemp)+1));
    strcpy(tokens[0], strtemp);
    tokens[0][strlen(strtemp)] = '\0';

    count = 1;
    while( (strtemp = strtok(NULL,"/")) != NULL )
    {
        tokens[count] = (char *)malloc(sizeof(char)*(strlen(strtemp)+1));
        strcpy(tokens[count], strtemp);
        tokens[count][strlen(strtemp)] = '\0';
        count++;
    }
    return count;
}
void inode_write_back(ilist *headptr)
{
    int offset;
    if(headptr->subdir !=NULL)
        inode_write_back(headptr->subdir);
    offset = s.ilistoff + headptr->i.inode_num*s.numiblk*BLKSIZE;
    if( lseek(fd, offset, SEEK_SET) == -1)
        perror("Error in lseek");
    if( write(fd, &headptr->i, sizeof(inode)) == -1)
        perror("Error in writing back the inode");
    if(headptr->next!=NULL)
        inode_write_back(headptr->next);
}

void disk_write_back()
{
    int numsbblks;
    numsbblks = 1 + (sizeof(superblock) - 1) / BLKSIZE;

    if( lseek(fd, BLKSIZE, SEEK_SET) == -1) // Skipping boot block
        perror("Error lseek disk_write_back");

    if( write(fd, &s, sizeof(superblock)) == -1) // writing superblock
        perror("Error writing Superblock disk_write_back");

    if( lseek(fd, s.ifreelstoff, SEEK_SET) == -1) // Moving the free list of inodes
        perror("Error lseek disk_write_back freelist of inodes");

    if( write(fd, freeinode, INODESIZE) == -1) // Writing the freelist of inodes
        perror("Error writing freelist of inodes");

    if( lseek(fd, s.ifreelstoff+(1+(INODESIZE-1)/BLKSIZE)*BLKSIZE, SEEK_SET) == -1)
        perror("Error lseek freeblklist disk_write_back");

    if( write(fd, freeblklist, freelistlen) == -1) //Writing the free blocks list
        perror("Error write freeblklist disk_write_back");

    inode_write_back(head);
}

ilist* listsrch(ilist *node, char *srchstr) // function returns the pointer to the node having
{                                           // srchstr filename
    ilist *temp;
    temp = node->subdir;
    while(temp!= NULL && strcmp(temp->i.fname, srchstr))
        temp = temp->next;
    return temp;
}

void removeilist(ilist *headptr)
{
    if(headptr->subdir !=NULL)
        removeilist(headptr->subdir);

    if(headptr->next!=NULL)
        removeilist(headptr->next);

    free(headptr);
}
int makeilist(ilist *node)
{
    int offset;
    ilist *temp;

    if(node->i.isubdir != -1)
    {
        node->subdir = (ilist *)malloc(sizeof(ilist));
        offset = s.ilistoff + s.numiblk * BLKSIZE * node->i.isubdir;
        if( lseek(fd, offset, SEEK_SET) == -1) return -1;
        if( read(fd, &node->subdir->i, sizeof(inode)) == -1) return -1;

        temp = node->subdir;
        while(temp->i.inext != -1)
        {
            temp->next = (ilist *)malloc(sizeof(ilist));
            temp->subdir = NULL;
            offset = s.ilistoff + s.numiblk * BLKSIZE * temp->i.inext;

            if( lseek(fd, offset, SEEK_SET) == -1) return -1;
            if( read(fd, &temp->next->i, sizeof(inode)) == -1) return -1;
            temp = temp->next;
        }
        temp->subdir = NULL;
        temp->next = NULL;
    }
    return 0; // Success
}

int createfilesystem() // returns the file descriptor of the created filesystem
{
    int fd, ctr;
    char buff[100];

    for(ctr = 0; ctr < 100; ctr++)
        buff[ctr]=(char)48;
    fd = open(FS_Name, O_CREAT|O_RDWR,  S_IRUSR | S_IWUSR);
    if(fd == -1)
    {
        perror("Error opening");
        exit(0);
    }
    for(ctr=0; ctr < (FILESYSSIZE/100); ctr++)
        if( write(fd, buff, sizeof(buff)) == -1)
        {
            perror("Error write");
            exit(0);
        }

    return fd;
}

void myformat()
{
    int fd, ctr, numsbblks, numiblk, usedblks, freeblks;
    char free = 'f', used = 'u';
    superblock sb;
    inode root;

    fd = createfilesystem(); // File system is created and filled with 0's
    if( lseek(fd, BLKSIZE, SEEK_SET) == -1) // Skipping BLKSIZE for boot block(which is already filled with 0's)
        perror("Error lseek boot block");

    numsbblks = 1 + (sizeof(superblock) - 1) / BLKSIZE;
    numiblk = 1 + (sizeof(inode)-1)/ BLKSIZE;

    usedblks = 1 + numsbblks + numiblk*INODESIZE + FREESPCSIZE/BLKSIZE ;
    freeblks = FILESYSSIZE/BLKSIZE - usedblks;
    // Initializing Super Block Starts
    strcpy(sb.filesysname, FS_Name);
    sb.filesyssize = FILESYSSIZE;
    sb.free_inode = INODESIZE - 1; // one for root inode.
    sb.used_blks = usedblks;
    sb.free_blks = freeblks;
    sb.rootinode = 0;
    sb.ilistoff = BLKSIZE*(1+numsbblks);
    sb.ifreelstoff = BLKSIZE*(1+numsbblks+numiblk*INODESIZE);
    sb.numiblk = numiblk;
        // -------- Other Things to be initialized later --------- Here ---------- //
    // Initializing Super Block Completes

    strcpy(root.fname, "root");
    root.type = 'd';
    root.size = 0;  // To be updated everytime
    root.creation_time = time(NULL);
    root.mode = 'w';
    root.inext = -1; // Indicator that no files and sub-directories are added
    root.isubdir = -1;
    root.inode_num = sb.rootinode;

    if( write(fd, &sb, sizeof(superblock)) == -1) // writing superblock
        perror("Error writing Superblock");

    if( lseek(fd, sb.ilistoff, SEEK_SET) == -1) // Going to starting of ilist which contains the root inode
        perror("Error lseek root inode format");

    if( write(fd, &root, sizeof(inode)) == -1)
        perror("Error writing root inode");

    if( lseek(fd, sb.ifreelstoff, SEEK_SET) == -1)
        perror("Error seeking freelist offset");

    if( write(fd, &used, 1) == -1 )
        perror("Error writing free space format");

    for(ctr = 1; ctr < FREESPCSIZE; ctr++)
    {
        if( write(fd, &free, 1) == -1 )
            perror("Error writing free space format");
    }
    close(fd);
}

int mymount(char *fsname)
{
    int numsbblks, ctr;

    if(mountflg)
    {
        mountflg = 0;
        numsbblks = 1 + (sizeof(superblock) - 1) / BLKSIZE;
        fd = open(fsname, O_RDWR,  S_IRUSR | S_IWUSR); // while mounting the filesystem can only be read.
        if(fd == -1)
        {
            printf("Please Format the File System Before the first use\n");
            return -1; // error
        }

        if( lseek(fd, BLKSIZE, SEEK_SET) == -1) // Skipping boot block
        {
            perror("Error lseek mount");
            return -1;
        }

        if( read(fd, &s, sizeof(superblock)) == -1)
        {
            perror("Error mount read superblock");
            return -1;
        }


        if( lseek(fd, s.ilistoff, SEEK_SET) == -1) // Going to starting of ilist which contains the root inode
        {
            perror("Error lseek root inode mount");
            return -1;
        }

        head = (ilist *)malloc(sizeof(ilist));
        if( read(fd, &head->i, sizeof(inode)) == -1)
        {
            perror("Error read root block mount");
            return -1;
        }
        head->next = NULL;
        head->subdir = NULL;
  /*      if(head->i.isubdir == -1)
            head->subdir = NULL;
        else
            if(makeilist(head) == -1)
            {
                perror("Error makelist mount");
                return -1;
            }*/

        if( lseek(fd, s.ifreelstoff ,SEEK_SET) == -1)
        {
            perror("Error lseek freeilist mount");
            return -1;
        }

        for(ctr = 0; ctr < INODESIZE; ctr++)
        {
            if( read(fd, &freeinode[ctr], 1) == -1 )
            {
                perror("Error read freeilist mount");
                return -1;
            }
        }

        freelistlen = FILESYSSIZE/BLKSIZE - (1 + numsbblks + s.numiblk*INODESIZE + FREESPCSIZE/BLKSIZE);
        freeblklist = (char *)malloc(sizeof(char) * freelistlen);

        if( lseek(fd, s.ifreelstoff+(1+(INODESIZE-1)/BLKSIZE)*BLKSIZE, SEEK_SET) == -1)
        {
            perror("Error lseek freeblklist mount");
            return -1;
        }

        for(ctr = 0; ctr < freelistlen; ctr++)
        {
            if( read(fd, &freeblklist[ctr], 1) == -1)
            {
                perror("Error read freeblklist mount");
                return -1;
            }
        }

        for(ctr = 0; ctr < MAXFILES; ctr++)
            oft[ctr].offset = -1;

        return 0; // Success
    }
    else
    {
        printf("Cannot Mount Already Mounted File-System\n");
        return -1;
    }
}

int unmount(char *fsname)
{
    if(!mountflg)
    {
        disk_write_back(); // writes back all cached entries to myFS
        mountflg = 1;
        if( close(fd) == -1)
        {
            perror("close fd unmount");
            return -1;
        }
        fd = 0;
        free(freeblklist);
        removeilist(head);
        return 0; // Success
    }
    else
    {
        printf("Error : File System Not Mounted\n");
        return -1;
    }
}

int myopen(char *fname, char mode)
{
    ilist *temp, *temp1;
    int ctr, count = 0;
    char **tokens;
    if(!mountflg)
    {
        for(ctr = 0; ctr < strlen(fname); ctr++)
            if(fname[ctr] == '/')
                count++;
            tokens = (char **)malloc(sizeof(char *)*count);
        count = stringparse(tokens, fname);
        if(strcmp(tokens[0],head->i.fname))
        {
            printf("Invalid Path\n");
            return -1;
        }

        temp = head;
        for(ctr = 1; ctr < count-1; ctr++)
        {
            if(temp->subdir == NULL)
                makeilist(temp);
            if( ( temp = listsrch(temp,tokens[ctr])) == NULL || temp->i.type == 'f')
            {
                printf("Invalid Path\n");
                return -1;
            }
        }
        if(temp->subdir == NULL)
            makeilist(temp);
        temp1 = temp;
        if( ( temp = listsrch(temp, tokens[count-1])) != NULL)
        {
            if(temp->i.mode=='r' && temp->i.mode != mode)
            {
                printf("File opened in wrong mode\n");
                return -1;
            }
        }
        else
        {
            temp = (ilist *)malloc(sizeof(ilist));
            temp->next = temp1->subdir;
            temp1->subdir = temp;
            temp->subdir = NULL;

            strcpy(temp->i.fname, tokens[count-1]);
            temp->i.fname[strlen(tokens[count-1])] = '\0';
            temp->i.type = 'f';
            temp->i.size = 0;
            temp->i.creation_time = time(NULL);
            temp->i.mode = mode;
            for(ctr = 0; ctr < NUMDIRECTBLK+NUMINDIRECTBLK; ctr++)
                temp->i.blklist[ctr] = -1;

            if(temp->next != NULL) temp->i.inext = temp->next->i.inode_num;
            else temp->i.inext = -1;

            temp->i.isubdir = -1;

            for(ctr = 0; ctr < INODESIZE; ctr++)
                if(freeinode[ctr] == 'f')
                    break;

            if(ctr == INODESIZE)
            {
                printf("Inode Limit Reached : Cannot Create File\n");
                return -1;
            }

            temp->i.inode_num = ctr;
            temp1->i.isubdir = ctr;
            freeinode[ctr] = 'u';
            s.free_inode--;
            disk_write_back();
        }

        for(ctr = 0; ctr < MAXFILES; ctr++)
            if(oft[ctr].offset == -1 )
                break;

        if(ctr == MAXFILES)
        {
            printf("Max Open File Limit Reached : Cannot Open File\n");
            return -1;
        }
        oft[ctr].inlst = temp;
        oft[ctr].path = malloc(strlen(fname)*sizeof(char));
        strcpy(oft[ctr].path , fname);
        oft[ctr].offset = 0;
        return ctr;
    }
    else
    {
        printf("Error : File System Not Mounted\n");
        return -1;
    }
}

int myread(int filedes, void *buf, int count)
{
    int ctr, ctr1, ctr2, blkno, offset, *blkoff, count1,count2,i,blk_num,temp4;
    if(!mountflg)
    {
        if(oft[filedes].offset == -1)
        {
            printf("Error : Bad File Descriptor\n");
            return -1;
        }

        if( oft[filedes].offset + count >= BLKSIZE * (NUMDIRECTBLK+NUMINDIRECTBLK*BLKSIZE/sizeof(int)) )
            count = BLKSIZE * (NUMDIRECTBLK+NUMINDIRECTBLK*BLKSIZE/sizeof(int)) - oft[filedes].offset;

        count1 = count;
        offset = oft[filedes].offset % BLKSIZE;
        blkno = oft[filedes].offset / BLKSIZE;
    count2 = (count+offset)/BLKSIZE +1;
        blkoff = (int *)malloc(sizeof(int)*(count2));
    if(blkno >= NUMDIRECTBLK)
      ctr1 = (blkno - NUMDIRECTBLK) * sizeof(int) / BLKSIZE + NUMDIRECTBLK ;
    else
      ctr1 = blkno;
    ctr2 =0;
    for(ctr=ctr1;ctr<NUMDIRECTBLK+NUMINDIRECTBLK && ctr2<count2;ctr++)
    {
            if(oft[filedes].inlst->i.blklist[ctr] == -1)
                break;
            if( ctr >=NUMDIRECTBLK && ctr <  NUMDIRECTBLK+NUMINDIRECTBLK )
            {
          temp4 = sizeof(int) * (blkno - (NUMDIRECTBLK + ( BLKSIZE* (ctr - NUMDIRECTBLK) /sizeof(int))));
          if(temp4 < 0)
        temp4 =0;
          if( lseek(fd, (oft[filedes].inlst->i.blklist[ctr])*BLKSIZE + s.ifreelstoff + FREESPCSIZE +temp4, SEEK_SET) == -1) // Skipping till the block
          {
          perror("Error lseek mount");
          return -1;
          }
          for(i=0;i<BLKSIZE/sizeof(int) && ctr2 < count2;i++)
          {
          if( read(fd, &blk_num, sizeof(blk_num)) == -1)
          {
              perror("Error mount read superblock");
              return -1;
          }
          if(blk_num == -1)
              break;
          else
              blkoff[ctr2++] = blk_num;
          }
            }
            else
          blkoff[ctr2++] = oft[filedes].inlst->i.blklist[ctr];
        }
        for(ctr1 = 0; ctr1 < ctr2; ctr1++)
        {
            if( lseek(fd,s.ifreelstoff+FREESPCSIZE+blkoff[ctr1]*BLKSIZE+offset, SEEK_SET) == -1)
            {
                perror("lseek block read");
                return -1;
            }

            if( ctr1 == ctr2 - 1)
            {
                if( read(fd, buf, count) == -1)
                {
                    perror("Error : read myread");
                    return -1;
                }
                buf = buf + count;
                oft[filedes].offset += count;
                count = 0;
            }
            else
            {
                if( read(fd, buf, BLKSIZE - offset) == -1)
                {
                    perror("Error : read myread");
                    return -1;
                }
                buf = buf + BLKSIZE - offset;
                oft[filedes].offset += BLKSIZE - offset;
                count = count + offset - BLKSIZE;
            }
            offset = 0;
        }
        return count1;
    }
    else
    {
        printf("Error : File System Not Mounted\n");
        return -1;
    }
}

int mywrite(int filedes, void *buf, int count)
{
    int ctr, ctr1, ctr2, blkno, offset, *blkoff, count1,i,j,blk_num,initial,temp1,temp2,temp4,size,cnt;
    ilist *temp3;
    char **tokens;
    if(!mountflg)
    {
        offset = oft[filedes].offset % BLKSIZE; // 0 to BLKSIZE-1 // offset inside a block
        blkno = oft[filedes].offset / BLKSIZE;  // Starts from 0 Blocks inside the file
        count1 = (count+offset)/BLKSIZE+1;
        blkoff = (int *)malloc(sizeof(int)*(count1));

        if(oft[filedes].inlst->i.mode == 'r')
        {
            perror("File opened in read mode:Permission denied\n");
            return -1;
        }

        if( oft[filedes].offset + count >= BLKSIZE * (NUMDIRECTBLK+NUMINDIRECTBLK*BLKSIZE/sizeof(int)) )
            count = BLKSIZE * (NUMDIRECTBLK+NUMINDIRECTBLK*BLKSIZE/sizeof(int)) - oft[filedes].offset;

        if(blkno >= NUMDIRECTBLK)
            ctr1 = (blkno - NUMDIRECTBLK) * sizeof(int) / BLKSIZE + NUMDIRECTBLK ;
        else
            ctr1 = blkno;
        ctr2 =0;
        for(ctr=ctr1;ctr<NUMDIRECTBLK+NUMINDIRECTBLK && ctr2<count1;ctr++)
        {
            if(oft[filedes].inlst->i.blklist[ctr] == -1)
            {
                for(j=0;j<freelistlen;j++)
                {
                    if(freeblklist[j] == 'f')
                    {
                        freeblklist[j] ='u';
                        s.free_blks--;
                        s.used_blks++;
                        break;
                    }
                }
                oft[filedes].inlst->i.blklist[ctr] = j;
                if( ctr >=NUMDIRECTBLK && ctr <  NUMDIRECTBLK+NUMINDIRECTBLK )
                {
                    initial =-1;
                    if(lseek(fd, j*BLKSIZE + s.ifreelstoff + FREESPCSIZE, SEEK_SET) == -1)
                    {
                        perror("Error:lseek write\n");
                        return -1;
                    }
                    for(j = 0 ; j<BLKSIZE/sizeof(int) ;j++)
                    {
                        if(write(fd, &initial , sizeof(initial)) == -1)
                        {
                            perror("Error:Write,in write\n");
                            return -1;
                        }
                    }
                }
            }
            if( ctr >=NUMDIRECTBLK && ctr <  NUMDIRECTBLK+NUMINDIRECTBLK )
            {
                temp4 = sizeof(int) * (blkno - (NUMDIRECTBLK + (BLKSIZE * (ctr - NUMDIRECTBLK) / sizeof(int))));
                if(temp4 < 0)
                    temp4 =0;
                if( lseek(fd, (temp1 = (oft[filedes].inlst->i.blklist[ctr])*BLKSIZE + s.ifreelstoff + FREESPCSIZE + temp4), SEEK_SET) == -1) // Skipping till the block
                {
                    perror("Error lseek write");
                    return -1;
                }
                for(i=0;i<BLKSIZE/sizeof(int) && ctr2<count1;i++)
                {
                    temp2 = i*sizeof(int) + temp1;
                    if( read(fd, &blk_num, sizeof(blk_num)) == -1)
                    {   
                        perror("Error write read");
                        return -1;
                    }
                    if(blk_num == -1)
                    {
                        for(j=0;j<freelistlen;j++)
                        {
                            if(freeblklist[j] == 'f')
                            {
                                freeblklist[j] ='u';
                                s.free_blks--;
                                s.used_blks++;
                                break;
                            }
                        }
                        if(lseek(fd, temp2, SEEK_SET) == -1)
                        {
                            perror("Error:lseek write\n");
                            return -1;
                        }

                        if(write(fd , &j , sizeof(j)) == -1)
                        {
                            perror("Error:Write in  write\n");
                            return -1;
                        }
                        blk_num = j;
                    }
                    blkoff[ctr2++] = blk_num;
                }
            }
            else
                blkoff[ctr2++] = oft[filedes].inlst->i.blklist[ctr];
        }
        j=0;
        for(i=0;i<ctr2-1;i++)
        {
            if(lseek(fd, blkoff[i]*BLKSIZE + s.ifreelstoff + FREESPCSIZE+offset, SEEK_SET) == -1)
            {
                perror("Error:lseek write\n");
                return -1;
            }
            if(write(fd , buf+j , BLKSIZE-offset) == -1)
            {
                perror("Error:Write in write\n");
                return -1;
            }
            j = j+ BLKSIZE - offset;
            oft[filedes].offset += BLKSIZE - offset;
            offset =0;
        } 
        if(lseek(fd, blkoff[i]*BLKSIZE + s.ifreelstoff + FREESPCSIZE+offset, SEEK_SET) == -1)
        {
            perror("Error:lseek write\n");
            return -1;
        }
        if(write(fd , buf + j , count - j) == -1)
        {
            perror("Error:Write in write\n");
            return -1;
        }
        oft[filedes].offset += count-j;
        size = offset + count - oft[filedes].inlst->i.size;
        if(size > 0)
        {
            for(ctr = 0; ctr < strlen(oft[filedes].path); ctr++)
                if(oft[filedes].path[ctr] == '/')
                    cnt++;
                tokens = (char **)malloc(sizeof(char *)*cnt);
            cnt = stringparse(tokens, oft[filedes].path);
            temp3 = head;
            for(ctr= 0 ;ctr<cnt;ctr++)
            {
                while(temp3 != NULL)
                {
                    if(strcmp(temp3->i.fname , tokens[ctr]) == 0)
                    {
                        temp3 -> i.size = temp3->i.size + size;
                        temp3 = temp3->subdir;
                        break;
                    }
                    else
                        temp3 = temp3->next;
                }
            }
        } 
        disk_write_back();
        return count;
    }
    else
    {
        printf("Error : File System Not Mounted\n");
        return -1;
    }
}

int myclose(int filedescriptor)
{
    if(!mountflg)
    {
        if(filedescriptor >=0 && filedescriptor < MAXFILES)
        {
            oft[filedescriptor].offset = -1;
            oft[filedescriptor].inlst = NULL;
            return 0;
        }
        else
        {
            printf("Error : Invalid File Descriptor\n");
            return -1;
        }
    }
    else
    {
        printf("Error : File System Not Mounted\n");
        return -1;
    }
}

int mymkdir(const char *dirname, mode_t mode)
{
    ilist *temp, *temp1;
    ilist *new_node;
    int ctr, count = 0;
    char **tokens;
    if(!mountflg)
    {
        for(ctr = 0; ctr < strlen(dirname); ctr++)
            if(dirname[ctr] == '/')
                count++;
        tokens = (char **)malloc(sizeof(char *)*count);
        count = stringparse(tokens, dirname);
        if(strcmp(tokens[0],head->i.fname))
        {
            printf("Invalid Path\n");
            return -1;
        }

        temp = head;
        for(ctr = 1; ctr < count-1; ctr++)
        {
            if(temp->subdir == NULL)
                makeilist(temp);
            if( ( temp = listsrch(temp,tokens[ctr])) == NULL || temp->i.type == 'f')
            {
                printf("Invalid Path\n");
                return -1;
            }
        }
        makeilist(temp);

        if( listsrch(temp, tokens[count-1]) != NULL)
        {
            printf("Directory or File Already Exists\n");
            return -1;
        }

        temp1 = temp->subdir;
        new_node = (ilist *)malloc(sizeof(ilist));
        temp->subdir = new_node;
        temp->subdir->next = temp1;
        temp->subdir->subdir = NULL;
        strcpy(temp->subdir->i.fname, tokens[count-1]);
        temp->subdir->i.fname[strlen(tokens[count-1])] = '\0';
        temp->subdir->i.type = 'd';
        temp->subdir->i.size = 0;
        temp->subdir->i.creation_time = time(NULL);
        temp->subdir->i.mode = 'w';

        if(temp->subdir->next != NULL) temp->subdir->i.inext = temp->subdir->next->i.inode_num;
        else temp->subdir->i.inext = -1;

        temp->subdir->i.isubdir = -1;

        for(ctr = 0; ctr < INODESIZE; ctr++)
            if(freeinode[ctr] == 'f')
                break;

        if(ctr == INODESIZE)
        {
            printf("Inode Limit Reached : Cannot Create Directory\n");
            return -1;
        }

        temp->subdir->i.inode_num = ctr;
        temp->i.isubdir = ctr;
        freeinode[ctr] = 'u';
        s.free_inode--;
        disk_write_back();
        return 0;
    }
    else
    {
        printf("Error : File System Not Mounted\n");
        return -1;
    }
}

int myrmdir(char *dirname)
{
    ilist *temp, *temp1, *prev_temp;
    int ctr, count = 0;
    char **tokens;
    if(!mountflg)
    {
        for(ctr = 0; ctr < strlen(dirname); ctr++)
            if(dirname[ctr] == '/')
                count++;
            tokens = (char **)malloc(sizeof(char *)*count);
        count = stringparse(tokens, dirname);

        if(count == 1)
        {
            printf("Cannot remove root\n");
            return -1;
        }

        if(strcmp(tokens[0],head->i.fname))
        {
            printf("Invalid Path\n");
            return -1;
        }

        temp = head;
        for(ctr = 1; ctr < count; ctr++)
        {
            prev_temp = temp;
            if(temp->subdir == NULL)
                makeilist(temp);
            if( ( temp = listsrch(temp,tokens[ctr])) == NULL || temp->i.type == 'f')
            {
                printf("Invalid Path\n");
                return -1;
            }
        }

        if(temp->i.isubdir != -1)
        {
            printf("Cannot Remove Directory : Directory Not Empty\n");
            return -ENOTEMPTY;
        }

        temp1 = prev_temp->subdir;
        if(temp1 == temp)
        {
            prev_temp->subdir = temp->next;
            if(temp->next == NULL)
                prev_temp->i.isubdir = -1;
            else
                prev_temp->i.isubdir = temp->next->i.inode_num;
        }

        while(temp1->next != NULL)
        {
            if(temp1->next == temp)
            {
                if(temp->next == NULL)
                    temp1->i.inext = -1;
                else
                    temp1->i.inext = temp->next->i.inode_num;
                temp1->next = temp->next;
            }
            temp1 = temp1->next;
        }
        freeinode[temp->i.inode_num] = 'f';
        free(temp);
        s.free_inode++;
        disk_write_back();
        return 0;
    }
    else
    {
        printf("Error : File System Not Mounted\n");
        return -1;
    }
}

int myrm(char *dirname)
{
    ilist *temp, *temp1, *prev_temp;
    int ctr, count = 0,blk_num,i,size;
    char **tokens;
    if(!mountflg)
    {
        for(ctr = 0; ctr < strlen(dirname); ctr++)
            if(dirname[ctr] == '/')
                count++;
        tokens = (char **)malloc(sizeof(char *)*count);
        count = stringparse(tokens, dirname);

        if(count == 1)
        {
            printf("Cannot remove root\n");
            return -1;
        }

        if(strcmp(tokens[0],head->i.fname))
        {
            printf("Invalid Path\n");
            return -1;
        }

        temp = head;
        for(ctr = 1; ctr < count; ctr++)
        {
            prev_temp = temp;
            if(temp->subdir == NULL)
                makeilist(temp);
            if( ( temp = listsrch(temp,tokens[ctr])) == NULL || (ctr != count-1 && temp->i.type == 'f'))
            {
                printf("Invalid Path\n");
                return -1;
            }
        }
        temp1 = prev_temp->subdir;
        if(temp1 == temp)
        {
            prev_temp->subdir = temp->next;
            if(temp->next == NULL)
                prev_temp->i.isubdir = -1;
            else
                prev_temp->i.isubdir = temp->next->i.inode_num;
        }

        while(temp1->next != NULL)
        {
            if(temp1->next == temp)
            {
                if(temp->next == NULL)
                    temp1->i.inext = -1;
                else
                    temp1->i.inext = temp->next->i.inode_num;
                temp1->next = temp->next;
            }
            temp1 = temp1->next;
        }
        freeinode[temp->i.inode_num] = 'f';
        s.free_inode++;
//code for blocks freing goes here
    for(ctr=0;ctr<NUMDIRECTBLK+NUMINDIRECTBLK;ctr++)
    {
      if(temp->i.blklist[ctr] == -1)
          break;
      if( ctr >=NUMDIRECTBLK && ctr <  NUMDIRECTBLK+NUMINDIRECTBLK )
      {
        if( lseek(fd, (temp->i.blklist[ctr])*BLKSIZE + s.ifreelstoff + FREESPCSIZE, SEEK_SET) == -1) // Skipping till the block
        {
          perror("Error lseek mount");
          return -1;
        }
        freeblklist[temp->i.blklist[ctr]] = 'f';
        s.used_blks--;
        s.free_blks++;
        for(i=0;i<BLKSIZE/sizeof(int);i++)
        {
          if( read(fd, &blk_num, sizeof(blk_num)) == -1)
          {
        perror("Error read");
        return -1;
          }
          if(blk_num == -1)
          break;
          else
          {
        freeblklist[blk_num] = 'f';
        s.used_blks--;
        s.free_blks++;
          }
        }
      }
      else
      {
          freeblklist[temp->i.blklist[ctr]] = 'f';
          s.used_blks--;
          s.free_blks++;
      }
      temp->i.blklist[ctr] =-1;
    }
        size = temp->i.size;
        temp1 = head;
        for(ctr= 0 ;ctr<count-1;ctr++)
        {
            while(temp1 != NULL)
            {
                if(strcmp(temp1->i.fname , tokens[ctr]) == 0)
                {
                    temp1 -> i.size = temp1->i.size - size;
                    temp1 = temp1->subdir;
                    break;
                }
                else
                    temp1 = temp1->next;
            }
    }
        free(temp);
        disk_write_back();
        return 0;
    }
    else
    {
        printf("Error : File System Not Mounted\n");
        return -1;
    }
}

int myls(const char *dirname)
{

    ilist *temp, *temp1;
    int ctr, count = 0;
    char **tokens;
    if(!mountflg)
    {
        temp = head;
        if(strcmp(dirname,"\0"))
        {
            for(ctr = 0; ctr < strlen(dirname); ctr++)
                if(dirname[ctr] == '/')
                    count++;
            tokens = (char **)malloc(sizeof(char *)*count);
            count = stringparse(tokens, dirname);
            if(strcmp(tokens[0],head->i.fname))
            {
                printf("Invalid Path\n");
                return -1;
            }


            for(ctr = 1; ctr < count; ctr++)
            {
                if(temp->subdir == NULL)
                    makeilist(temp);
                if( ( temp = listsrch(temp,tokens[ctr])) == NULL || temp->i.type == 'f')
                {
                    printf("Invalid Path\n");
                    return -1;
                }
            }
        }
        makeilist(temp);
        temp1 = temp->subdir;
        //printf("Name\t\tType\tsize\tMode\tTime\n");
        while(temp1!=NULL)
        {
            printf("%s\t%c\t%d\t%c\t%s",temp1->i.fname,temp1->i.type,temp1->i.size,temp1->i.mode,ctime(&temp1->i.creation_time));
            temp1=temp1->next;
        }
        return 0;
    }
    else
    {
        printf("Error : File System Not Mounted\n");
        return -1;
    }
}

int getfileattr(const char *dirname, struct stat  *statbuf)
{
    ilist *temp;
    int ctr, count = 0;
    char **tokens;
    if(!mountflg)
    {
        for(ctr = 0; ctr < strlen(dirname); ctr++)
            if(dirname[ctr] == '/')
                count++;
            tokens = (char **)malloc(sizeof(char *)*count);
        count = stringparse(tokens, dirname);
        if(strcmp(tokens[0],head->i.fname))
        {
            printf("Invalid Path\n");
            return -1;
        }
        
        temp = head;
        for(ctr = 1; ctr < count; ctr++)
        {
            if(temp->subdir == NULL)
                makeilist(temp);
            if( ( temp = listsrch(temp,tokens[ctr])) == NULL)
            {
                if(ctr == count-1)
                    return -ENOENT;
                printf("Invalid Path\n");
                return -1;
            }
        }
        statbuf->st_ino = temp->i.inode_num;
        statbuf->st_size = temp->i.size;
        statbuf->st_ctime = temp->i.creation_time;
        if(temp->i.type == 'd')
        {
            statbuf->st_mode = S_IFDIR | 0755;
            statbuf->st_nlink = 2;
        }
        else
        {
            statbuf->st_mode = S_IFREG | 0444;
            statbuf->st_nlink = 1;
        }
        return 0;
    }
    else
    {
        printf("Error : File System Not Mounted\n");
        return -1;
    }
}

ilist* getopendir(const char *dirname)
{
    ilist *temp;
    int ctr, count = 0;
    char **tokens;
    if(!mountflg)
    {
        for(ctr = 0; ctr < strlen(dirname); ctr++)
            if(dirname[ctr] == '/')
                count++;
            tokens = (char **)malloc(sizeof(char *)*count);
        count = stringparse(tokens, dirname);
        if(strcmp(tokens[0],head->i.fname))
        {
            printf("Invalid Path\n");
            return NULL;
        }

        temp = head;
        for(ctr = 1; ctr < count; ctr++)
        {
            if(temp->subdir == NULL)
                makeilist(temp);
            if( ( temp = listsrch(temp,tokens[ctr])) == NULL || temp->i.type == 'f')
            {
                printf("Invalid Path\n");
                return NULL;
            }
        }
        return temp;
    }
    else
    {
        printf("Error : File System Not Mounted\n");
        return NULL;
    }
}

int changemode(const char *dirname,char mode)
{
    ilist *temp;
    int ctr, count = 0;
    char **tokens;
    if(!mountflg)
    {
        for(ctr = 0; ctr < strlen(dirname); ctr++)
            if(dirname[ctr] == '/')
                count++;
        tokens = (char **)malloc(sizeof(char *)*count);
        count = stringparse(tokens, dirname);
        if(strcmp(tokens[0],head->i.fname))
        {
            printf("Invalid Path\n");
            return -1;
        }

        temp = head;
        for(ctr = 1; ctr < count; ctr++)
        {
            if(temp->subdir == NULL)
                makeilist(temp);
            if( ( temp = listsrch(temp,tokens[ctr])) == NULL)
            {
                printf("Invalid Path\n");
                return -1;
            }
        }
        temp->i.mode = mode;
        return 0;
    }
    else
    {
        printf("Error : File System Not Mounted\n");
        return -1;
    }
}


static void fs_fullpath(char fpath[PATH_MAX], const char *path)
{
    strcpy(fpath, "/root");
    strncat(fpath, path, PATH_MAX); // ridiculously long paths will
    // break here
}


void *fs_init(struct fuse_conn_info *conn)
{
    int retstat = 0;
    printf("---------In init-------\n");
    myformat();
    retstat = mymount(FS_Name);
    if(retstat == -1)
        printf("Error in mounting\n");
    else
        printf("Successfully mounted filesystem\n");
  //  return FS_DATA;
    return FS_DATA;
}

void fs_destroy(void *userdata)
{
    printf("---------In destroy-------\n");
    unmount(FS_Name);
}

int fs_getattr(const char *path, struct stat *statbuf)
{
    printf("---------In getattr-------\n");
    int retstat = 0;
    char fpath[PATH_MAX];
    
    fs_fullpath(fpath, path);
    printf(" [fpath] %s\n", fpath);
    
    retstat = getfileattr(fpath, statbuf);
    if (retstat != 0)
        fprintf(stderr,"Error in getting file attributes\n");

    printf("Returning from getattr with retstat : %d\n", retstat);
    return retstat;
}

int fs_mkdir(const char *path, mode_t mode)
{
    printf("---------In mkdir-------\n");
    int retstat = 0;
    char fpath[PATH_MAX];
    
    fs_fullpath(fpath, path);
    
    retstat = mymkdir(fpath, mode);
    if (retstat < 0)
    {
        fprintf(stderr,"Error in creating directory\n");
    } 
    
    return retstat;
}

int fs_opendir(const char *path, struct fuse_file_info *fi)
{
    ilist *ptr;

    printf("---------In opendir-------\n");
    char fpath[PATH_MAX];
    int retstat = 0;
    
    fs_fullpath(fpath, path);
    printf("[fpath] : %s\n", fpath);
    ptr = getopendir(fpath);

    if(ptr == NULL)
    {
        retstat = -1;
        printf("Error in getopendir\n");
    }
    
    printf("Pointing to directory name : %s\n", ptr->i.fname);
    fi->fh = (intptr_t) ptr;
    
    return retstat;
}

int fs_access(const char *path, int mask)
{
    printf("---------In access-------\n");
    int retstat = 0;
    char fpath[PATH_MAX];
    
    fs_fullpath(fpath, path);
    return retstat;
}

int fs_releasedir(const char *path, struct fuse_file_info *fi)
{
    printf("---------In releasedir-------\n");
    printf("[path] %s\n", path);
    int retstat = 0;
    
    fi->fh = (intptr_t) NULL;
    return retstat;
}

int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset,
               struct fuse_file_info *fi)
{
    printf("---------In readdir-------\n");
    printf("[path] : %s\n", path);
    int retstat = 0;
    ilist *ptr, *temp;
    ptr = (ilist *) (uintptr_t) fi->fh;
    
    if(ptr->subdir == NULL)
        retstat = makeilist(ptr);
    
    if (retstat == -1) {
        printf("Error in readdir\n");
        return retstat;
    }
    temp = ptr->subdir;
    // This will copy the entire directory into the buffer.  The loop exits
    // when either the system readdir() returns NULL, or filler()
    // returns something non-zero.  The first case just means I've
    // read the whole directory; the second means the buffer is full.
    while(temp!=NULL)
    {
        printf("calling filler with name %s\n", temp->i.fname);
        if (filler(buf, temp->i.fname, NULL, 0) != 0)
        {
            printf("ERROR fs_readdir filler:  buffer full\n");
            return -ENOMEM;
        }
        temp = temp->next;
    }
    return retstat;
}

int fs_rmdir(const char *path)
{
    printf("---------In rmdir-------\n");
    int retstat = 0;
    char fpath[PATH_MAX];
    
    fs_fullpath(fpath, path);
    
    retstat = myrmdir(fpath);
    if (retstat < 0)
        printf("Error in rmdir\n");
    return retstat;
}

int fs_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    printf("---------In create-------\n");
    int retstat = 0;
    char fpath[PATH_MAX], acmode;
    int fd1;
    
    fs_fullpath(fpath, path);

    printf("mode : %d %d\n", mode, O_ACCMODE<<7);
    if( (mode & O_ACCMODE<<7) == O_RDONLY )
        acmode = 'r';
    else
        acmode = 'w';

    printf("[%s] created in mode %c\n", path, acmode);
    fd1 = myopen(fpath, acmode);
    printf("File des : %d\n", fd1);
    if (fd1 < 0)
    {
        retstat = fd1;
        printf("Error in create\n");
    }
    fi->fh = fd1;

    return retstat;
}

int fs_fgetattr(const char *path, struct stat *statbuf, struct fuse_file_info *fi)
{
    printf("---------In fgetattr-------\n");
    int retstat = 0, fd1 ;
    ilist *temp;

    fd1 = fi->fh;
    temp = oft[fd1].inlst;

    statbuf->st_ino = temp->i.inode_num;
    statbuf->st_size = temp->i.size;
    statbuf->st_ctime = temp->i.creation_time;
    if(temp->i.type == 'd')
    {
        statbuf->st_mode = S_IFDIR | 0755;
        statbuf->st_nlink = 2;
    }
    else
    {
        statbuf->st_mode = S_IFREG | 0444;
        statbuf->st_nlink = 1;
    }

    return retstat;
}

int fs_flush(const char *path, struct fuse_file_info *fi)
{
    printf("---------In flush-------\n");
    int retstat = 0;

    return retstat;
}

int fs_utime(const char *path, struct utimbuf *ubuf) {return 0;}

int fs_release(const char *path, struct fuse_file_info *fi)
{
    printf("---------In release-------\n");
    int retstat = 0;
    retstat = myclose(fi->fh);
    return retstat;
}

int fs_open(const char *path, struct fuse_file_info *fi)
{
    printf("---------In open-------\n");
    int retstat = 0;
    char fpath[PATH_MAX], acmode;
    int fd1;
    
    fs_fullpath(fpath, path);

    printf("mode : %d %d\n", fi->flags, O_ACCMODE);
    if( (fi->flags & O_ACCMODE<<7) == O_RDONLY )
        acmode = 'r';
    else
        acmode = 'w';

    printf("[%s] opened in mode %c\n", path, acmode);
    fd1 = myopen(fpath, acmode);
    printf("File des : %d\n", fd1);
    if (fd1 < 0)
    {
        retstat = fd1;
        printf("Error in create\n");
    }
    fi->fh = fd1;
    
    return retstat;
}

int fs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    printf("---------In read-------\n");
    int retstat = 0;
    
    retstat = myread(fi->fh, (void *)buf, (int)size);
    printf("restat : %d", retstat);
    if (retstat < 0)
    {
        printf("Error in fs_read\n");
    }
    
    return retstat;
}

int fs_write(const char *path, const char *buf, size_t size, off_t offset,
             struct fuse_file_info *fi)
{
    printf("---------In write-------\n");
    int retstat = 0;

    retstat = mywrite(fi->fh, (void *)buf, (int)size);
    if (retstat < 0)
        printf("Error in fs_write\n");
        
    return retstat;
}

int fs_unlink(const char *path)
{
    printf("---------In unlink-------\n");
    int retstat = 0;
    char fpath[PATH_MAX];

    fs_fullpath(fpath, path);
    
    retstat = myrm(fpath);
    if (retstat < 0)
    {
        printf("Error in removing file\n");
    }
    return retstat;
}

int fs_chmod(const char *path, mode_t mode)
{
    printf("---------In chmod-------\n");
    int retstat = 0;
    char fpath[PATH_MAX], acmode;
    
    fs_fullpath(fpath, path);

    if( (mode & O_ACCMODE<<7) == O_RDONLY )
        acmode = 'r';
    else
        acmode = 'w';
    
    retstat = changemode(fpath, acmode);
    if (retstat < 0)
    {
        printf("Error in chmod\n");
    }
    return retstat;
}


struct fuse_operations fs_oper = {
    .getattr = fs_getattr,
    /*.readlink = bb_readlink,
    // no .getdir -- that's deprecated
    .getdir = NULL,
    .mknod = bb_mknod,*/
    .mkdir = fs_mkdir,
    .unlink = fs_unlink,
    .rmdir = fs_rmdir,
 /*   .symlink = bb_symlink,
    .rename = bb_rename,
    .link = bb_link,*/
    .chmod = fs_chmod,
 /*   .chown = bb_chown,
    .truncate = bb_truncate,*/
    .utime = fs_utime,
    .open = fs_open,
    .read = fs_read,
    .write = fs_write,
    /* Just a placeholder, don't set */ // huh???
    // .statfs = bb_statfs,
     .flush = fs_flush,
     .release = fs_release,
   /*  .fsync = bb_fsync,
     .setxattr = bb_setxattr,
     .getxattr = bb_getxattr,
     .listxattr = bb_listxattr,
     .removexattr = bb_removexattr,*/
     .opendir = fs_opendir,
     .readdir = fs_readdir,
     .releasedir = fs_releasedir,
  /*   .fsyncdir = bb_fsyncdir,*/
     .init = fs_init,
     .destroy = fs_destroy,
     .access = fs_access,
     .create = fs_create,
  //   .ftruncate = bb_ftruncate,
     .fgetattr = fs_fgetattr
};

int main(int argc, char *argv[])
{
    int fuse_stat;
    umask(0);
    fprintf(stderr, "about to call fuse_main\n");
    fuse_stat = fuse_main(argc, argv, &fs_oper, NULL);
    fprintf(stderr, "fuse_main returned %d\n", fuse_stat);
    return 0;
}

